package com.kimheang.article.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.github.javafaker.Faker;
import com.kimheang.article.model.Article;
import com.kimheang.article.model.ArticleFilter;
import com.kimheang.article.model.ArticlePagination;
import com.kimheang.article.model.Category;
import com.kimheang.article.service.ArticleService;
import com.kimheang.article.service.CategoryService;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
	@Value("${local.server.path}")
	private String serverPath;
	@Value("${local.client.path}")
	private String clientPath;
	@Autowired
	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}

	@GetMapping("/article")
	public String article(ArticleFilter filter,ModelMap model) {
		List<Article> articles =articleService.findAllFilter(filter);
		List<Article> articlesAllList = articleService.findByAll(filter); 
		List<Category> categories = categoryService.findAll(); 
		model.addAttribute("options", categories);
		model.addAttribute("article", new Article());
		model.addAttribute("filter",filter);
		if(filter.getPage()==null)
			filter.setPage(1);
		if(filter.getLimit()==null)
			filter.setLimit(10);
		model.addAttribute("currentPage",filter.getPage());
		model.addAttribute("articlePage",(filter.getPage()));
		double size=Math.ceil((double)articlesAllList.size()/(double)filter.getLimit());
		System.out.println(size);
		model.addAttribute("articlesList", (int)size);
		model.addAttribute("articles",articles);
		return "article/article";
	}
	@GetMapping("/article/page/{page}")
	public String articlePage(ModelMap model,@PathVariable("page") int page) {
		model.addAttribute("articlePage", page);
		List<Category> categories = categoryService.findAll();
		model.addAttribute("options", categories);
		model.addAttribute("article", new Article());
		if(page==1) page=0;
		List<Article> articles =articleService.findAll((10*page)-10);
		double size=Math.ceil(articleService.findAllList().size()/10.0);
		model.addAttribute("articlesList", (int)size);
		model.addAttribute("articles",articles);
		return "article/article";
	}
	
	@GetMapping("/add")
	public String addNewArticle(Model m) {
		m.addAttribute("formAdd", true);
		List<Category> categories = categoryService.findAll();
		m.addAttribute("options", categories);
		m.addAttribute("article", new Article());
		return "article/add";
	}
	
	@GetMapping("/article/{id}")
	public String articleFindOne(@PathVariable("id") int id,ModelMap model) {
		model.addAttribute("article",articleService.findOne(id));
		return "article/article_detail";
	}
	
	@PostMapping("/add")
	public String saveArticle(@RequestParam("image") MultipartFile image,@Valid @ModelAttribute Article article,BindingResult bindingResult,Model m) {
		String realFileName = "";
		if (bindingResult.hasErrors()|| image.isEmpty()) {
			m.addAttribute("formAdd", true);
			List<Category> categories = categoryService.findAll();
			m.addAttribute("options", categories);
            return "article/add";
        }
		try {
			String[] extensions=image.getOriginalFilename().split("\\.");
			String extension=extensions[1];
			realFileName = UUID.randomUUID()+"."+extension;
			File directory = new File(serverPath);
			if (! directory.exists()){
				directory.mkdir();
			}
			Files.copy(image.getInputStream(), Paths.get(serverPath, realFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		double size=Math.ceil(articleService.findAllList().size()/10.0);
		Category category = categoryService.findOne(article.getCategory().getId());
		article.setCategory(category);
		article.setCreatedDate(new Date().toString());
		article.setThumbnail(clientPath+realFileName);
		articleService.add(article);
		return "redirect:/article"; 
	}
	String oldFile="";
	@GetMapping("/update/{id}")
	public String updateArticle(@PathVariable("id") int id,ModelMap model) {
		model.addAttribute("formAdd", false);
		Article article = articleService.findOne(id);
		oldFile = article.getThumbnail();
		List<Category> categories = categoryService.findAll();
		model.addAttribute("options", categories);
		model.addAttribute("article",article);
		return "article/add";
	}
	
	@PostMapping("/update")
	public String update(@RequestParam("image") MultipartFile image,@Valid @ModelAttribute Article article,BindingResult bindingResult,Model m) {
		String realFileName = "";
		boolean isDelete=false;
		if(bindingResult.hasErrors()) {
			m.addAttribute("formAdd", false);
			List<Category> categories = categoryService.findAll();
			m.addAttribute("options", categories);
            return "article/add";
		}
		if(!image.isEmpty()) {
			try {
				String[] extensions=image.getOriginalFilename().split("\\.");
				String extension=extensions[1];
				realFileName = UUID.randomUUID()+"."+extension;
				File directory = new File(serverPath);
				if (! directory.exists()){
					directory.mkdir();
				}
				Files.copy(image.getInputStream(), Paths.get(serverPath, realFileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
			article.setThumbnail(clientPath+realFileName);
			isDelete=true;
		}
		else 
			article.setThumbnail(oldFile);
		Category category = categoryService.findOne(article.getCategory().getId());
		article.setCreatedDate(new Date().toString());
		article.setCategory(category);
		
		articleService.update(article,oldFile,isDelete);
		return "redirect:/article";
	}
	
	@GetMapping("/delete/{id}")
	public String removeArticle(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
	
	@GetMapping("/article/search")
	public String articleSearch(@RequestParam("title") String title,ModelMap model) {
		List<Article> articles =articleService.findBytitle((title+"%").toUpperCase());
		articles.forEach(System.out::println);
		List<Category> categories = categoryService.findAll();
		model.addAttribute("options", categories);
		model.addAttribute("article", new Article());
		if(articles.size()<1) {
			model.addAttribute("articlePage",  "0 (Not Found! )" );
			model.addAttribute("articlesList", 0);
		}
		else {
			model.addAttribute("articlePage", 1);
			double size=Math.ceil(articleService.findBytitle((title+"%").toUpperCase()).size()/10.0);
			model.addAttribute("articlesList", (int)size);
		}
		model.addAttribute("articles",articles);
		return "article/article";
	}
	
	@GetMapping("/article/filter")
	public String articleFilter(@RequestParam("category.id") int cateId,ModelMap model) {
		List<Article> articles =articleService.findByCategory(cateId);
		List<Category> categories = categoryService.findAll();
		model.addAttribute("options", categories);
		model.addAttribute("article", new Article());
		if(articles.size()<1) {
			model.addAttribute("articlePage", "0 (Not Found! )");
			model.addAttribute("articlesList", 0);
		}
		else {
			model.addAttribute("articlePage", 1);
			double size=Math.ceil(articleService.findByCategory(cateId).size()/10.0);
			model.addAttribute("articlesList", (int)size);
		}
		model.addAttribute("articles",articles);
		return "article/article";
	}
}
