package com.kimheang.article.controller.category;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.kimheang.article.model.Category;
import com.kimheang.article.service.CategoryService;

@Controller
public class CategoryController {
	private CategoryService categoryService;
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	@GetMapping("/article/category")
	public String category(ModelMap m) {
		m.addAttribute("save", true);
		m.addAttribute("categories", categoryService.findAll());
		m.addAttribute("category", new Category());
		return "category/category";
	}
	@PostMapping("/article/category/save")
	public String saveCategory(@Valid @ModelAttribute Category category,BindingResult bindingResult,ModelMap m) {
		if(bindingResult.hasErrors()) {
			return "redirect:/article/category";
		}
		categoryService.add(category);
		return "redirect:/article/category";
	}
	
	@PostMapping("/article/category/update")
	public String updateCategory(@ModelAttribute Category category) {
		categoryService.update(category);
		return "redirect:/article/category";
	}
	
	@GetMapping("/article/category/delete/{id}")
	public String removeCategory(@PathVariable("id") int id) {
		categoryService.delete(id);
		return "redirect:/article/category";
	}
	
	@GetMapping("/article/category/find/{id}")
	public String findCategory(@PathVariable("id") int id,ModelMap m) {
		m.addAttribute("save", false);
		m.addAttribute("category", categoryService.findOne(id));
		m.addAttribute("categories", categoryService.findAll());
		return "category/category";
	}
}
