package com.kimheang.article.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadFilController {
	@GetMapping("/upload")
	public String uploadFile() {
		return "upload";
	}
	
	@PostMapping("/upload")
	public String saveFile(@RequestParam("file") MultipartFile file) {
		System.out.println(file.getOriginalFilename());
		String serverPath="C:\\Users\\Kimheang\\Music\\UploadImage";
		if(!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(),Paths.get(serverPath,file.getOriginalFilename()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return"redirect:/upload";
	}
}
