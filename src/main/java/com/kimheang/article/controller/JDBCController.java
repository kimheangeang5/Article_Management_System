package com.kimheang.article.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kimheang.article.repository.CateJDBCRepos;

//@Controller
public class JDBCController {
	@Autowired
	private CateJDBCRepos cateJDBCRepos;
	@GetMapping("/cate")
	@ResponseBody
	public String showAllCategories() {
		cateJDBCRepos.showAllCategories();
		return "Testing JDBC";
	}
}
