package com.kimheang.article;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticleMangementSystemApplication {
	public static void main(String[] args) {
		SpringApplication.run(ArticleMangementSystemApplication.class, args);
	} 
}
