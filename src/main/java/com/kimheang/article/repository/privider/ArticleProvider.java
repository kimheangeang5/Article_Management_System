package com.kimheang.article.repository.privider;

import org.apache.ibatis.jdbc.SQL;

import com.kimheang.article.model.ArticleFilter;

public class ArticleProvider {
	public String findAllFilter(ArticleFilter filter) {
		return new SQL() {{
			SELECT("a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name");
			FROM("tb_articles a");
			INNER_JOIN("tb_categories c ON a.cate_id = c.id");
			if(filter.getTitle()!=null)
				WHERE("a.title ILIKE '%'|| #{title} ||'%'");
			if(filter.getCateId()!=null)
				WHERE("a.cate_id ILIKE #{cateId}");
			if(filter.getLimit()!=null)
				ORDER_BY("a.id LIMIT #{limit} OFFSET (#{page}*#{limit})-#{limit}");
			else
				ORDER_BY("a.id LIMIT 10 OFFSET (#{page}*10)-10");
		
		}}.toString();
	}
	 
	public String findByAll(ArticleFilter filter) {
		return new SQL() {{
			SELECT("a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name");
			FROM("tb_articles a");
			INNER_JOIN("tb_categories c ON a.cate_id = c.id");
			if(filter.getTitle()!=null)
				WHERE("a.title ILIKE '%'|| #{title} ||'%'");
			if(filter.getCateId()!=null)
				WHERE("a.cate_id=#{cateId}");
		}}.toString();
	}
}
