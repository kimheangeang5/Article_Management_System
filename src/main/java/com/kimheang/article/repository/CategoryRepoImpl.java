package com.kimheang.article.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.javafaker.Faker;
import com.kimheang.article.model.Category;

//@Repository
public class CategoryRepoImpl implements CategoryRepository{

	private List<Category> categories = new ArrayList<>();
	
	public CategoryRepoImpl() {
		Faker faker = new Faker();
		for(int i=1;i<=5;i++) {
			categories.add(new Category(i,faker.job().position()));
		}
	}
	
	@Override
	public void add(Category category) {
		categories.add(category);		
	}

	@Override
	public List<Category> findAll() {
		return categories;
	}

	@Override
	public Category findOne(int id) {
		int i=0;
		for(Category a : categories) {
			if(a.getId()==id) {
		        return a;
			}
		}
		return null;
	}

	@Override
	public void update(Category category) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

}
