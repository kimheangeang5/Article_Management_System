package com.kimheang.article.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.kimheang.article.model.Category;

@Repository
public interface CategoryRepository {
	
	@Insert("INSERT INTO tb_categories (name) VALUES(#{Name})")
	public void add(Category category);
	
	@Update("UPDATE tb_categories SET name = #{Name} WHERE id = #{id}")
	public void update(Category category);
	
	@Delete("DELETE FROM tb_categories WHERE id = #{id}")
	public void delete(int id);
	
	@Select("SELECT id,name FROM tb_categories")
	@Results({
		@Result(property="Name",column="name")
	})
	public List<Category> findAll();
	
	@Select("SELECT id,name FROM tb_categories WHERE id = #{id}")
	@Results({
		@Result(property="Name",column="name")
	})
	public Category findOne(int id);
}
