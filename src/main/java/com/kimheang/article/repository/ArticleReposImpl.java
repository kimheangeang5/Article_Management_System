package com.kimheang.article.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.javafaker.Faker;
import com.kimheang.article.model.Article;
import com.kimheang.article.model.ArticleFilter;
import com.kimheang.article.model.ArticlePagination;
import com.kimheang.article.model.Category;

//@Repository
public class ArticleReposImpl implements ArticleRepository{
	List<Article> articles = new ArrayList<>();
	@Override
	public void add(Article article) {
		articles.add(article);
	}

	@Override
	public Article findOne(int id) {
		for(Article a:articles) {
			if(a.getId()==id) {
				return a;
			}
		}
		return null;
	}

	@Override
	public List<Article> findAll(int page) {
		return articles;
	}
	public ArticleReposImpl() {
		Faker faker = new Faker();
		for(int i =1;i<=10;i++) {
			articles.add(new Article(i,faker.book().title(),faker.book().title(),faker.name().fullName(),faker.internet().image(200, 200, false, null),new Category(i,faker.job().field()),new Date().toString()));		
		}
	}
	@Override
	public void update(Article article) {
		for(int i = 0; i<articles.size();i++) {
			if(articles.get(i).getId()==article.getId()) {
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setCategory(article.getCategory());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setAuthor(article.getAuthor());
				articles.get(i).setCategory(article.getCategory());
				articles.get(i).setThumbnail(article.getThumbnail());
			}
		}
	}
	@Override
	public void delete(int id) {
		for(int i = 0; i<articles.size();i++) {
			if(articles.get(i).getId()==id) {
				Article a = findOne(id);
				articles.remove(a);
			}
		}
	}

	@Override
	public List<Article> findAllList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Article> findByCategory(int cateId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Article> findAllFitler(ArticleFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Article> findByTitle(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Article> findByAll(ArticleFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
