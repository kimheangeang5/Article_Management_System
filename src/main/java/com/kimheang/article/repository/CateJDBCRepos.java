package com.kimheang.article.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//@Repository
public class CateJDBCRepos {
	
	@Autowired
	private DataSource dataSource;
	
	public void showAllCategories() {
		try {
			Connection conn = dataSource.getConnection();
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM tb_categories";
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
