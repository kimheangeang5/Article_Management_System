package com.kimheang.article.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.kimheang.article.model.Article;
import com.kimheang.article.model.ArticleFilter;
import com.kimheang.article.model.ArticlePagination;
import com.kimheang.article.repository.privider.ArticleProvider;

@Repository
public interface ArticleRepository {
	@Insert("INSERT INTO tb_articles (title,description,author,thumbnail,created_date,cate_id) "
			+ "VALUES(#{title},#{description},#{author},#{thumbnail},#{createdDate},#{category.id})")
	public void add(Article article); 
	
	@Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name FROM "
			+ "tb_articles a INNER JOIN tb_categories c ON a.cate_id = c.id WHERE a.id = #{id}")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public Article findOne(int id);
	
	@Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name FROM "
			+ "tb_articles a INNER JOIN tb_categories c ON a.cate_id = c.id WHERE a.title ILIKE #{title}")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findByTitle(String title);
	
	
	@SelectProvider(method="findByAll",type=ArticleProvider.class)
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findByAll(ArticleFilter filter);
	
	@Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name FROM "
			+ "tb_articles a INNER JOIN tb_categories c ON a.cate_id = c.id WHERE a.cate_id = #{cateId}")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findByCategory(int cateId);
	
	
	
	@Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name FROM "
			+ "tb_articles a INNER JOIN tb_categories c ON a.cate_id = c.id ORDER BY a.id ASC LIMIT 10 OFFSET #{page}")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findAll(int page);
	
	@Select("SELECT a.id,a.title,a.description,a.author,a.thumbnail,a.created_date,a.cate_id,c.name as cate_name FROM "
			+ "tb_articles a INNER JOIN tb_categories c ON a.cate_id = c.id ORDER BY a.id ASC")
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findAllList();

	@Update("UPDATE tb_articles SET title = #{title}, description=#{description},author=#{author},thumbnail=#{thumbnail},created_date=#{createdDate},cate_id=#{category.id} WHERE id = #{id}")
	public void update(Article article);
	
	@Delete("DELETE FROM tb_articles WHERE id=#{id}")
	public void delete(int id);
	
	@SelectProvider(method="findAllFilter",type=ArticleProvider.class)
	@Results({
		@Result(property="createdDate",column="created_date"),
		@Result(property="category.id",column="cate_id"),
		@Result(property="category.name",column="cate_name")
	})
	public List<Article> findAllFitler(ArticleFilter filter);
}
