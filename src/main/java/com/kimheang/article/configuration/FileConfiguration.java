package com.kimheang.article.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@PropertySource("classpath:ams.properties")
public class FileConfiguration implements WebMvcConfigurer{
	@Value("${local.server.path}")
	private String serverPath;
	@Value("${local.client.path}")
	private String clientPath;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(clientPath+"**").addResourceLocations("file:"+serverPath);
	}
}
