package com.kimheang.article.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.kimheang.article.model.Article;
import com.kimheang.article.model.ArticleFilter;
import com.kimheang.article.model.ArticlePagination;
import com.kimheang.article.repository.ArticleRepository;

@Service
@PropertySource("classpath:ams.properties")
public class ArticleServiceImplementation implements ArticleService{
	@Value("${local.server.path}")
	private String serverPath;
	@Value("${local.client.path}")
	private String clientPath;
	private ArticleRepository articleRepo;

	@Autowired
	public void setArticle(ArticleRepository article) {
		this.articleRepo = article;
	}

	@Override
	public void add(Article article) {
		articleRepo.add(article);
	}

	@Override
	public Article findOne(int id) {
		return articleRepo.findOne(id);
	}

	@Override
	public List<Article> findAll(int page) {
		return articleRepo.findAll(page);
	}

	@Override
	public void update(Article article,String oldFile,boolean isDelete) {
		if(isDelete==true) {
			if(oldFile.indexOf("/Image")!=-1) {
				String[] names=oldFile.split("Image/");
				String name=names[1];
				String file=serverPath+name;
				try {
					Files.delete(Paths.get(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
		articleRepo.update(article);
	}

	@Override
	public void delete(int id) {
		Article a = articleRepo.findOne(id);
		if(a.getThumbnail().indexOf("Image/")!=-1) {
			String[] names=a.getThumbnail().split("Image/");
			String name=names[1];
			String file=serverPath+name;
			try {
				Files.delete(Paths.get(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		articleRepo.delete(id);
	}

	@Override
	public List<Article> findAllList() {
		return articleRepo.findAllList();
	}

	@Override
	public  List<Article> findBytitle(String title) {
		return articleRepo.findByTitle(title);
	}

	@Override
	public  List<Article> findByCategory(int cateId) {
		return articleRepo.findByCategory(cateId);
	}

	@Override
	public List<Article> findAllFilter(ArticleFilter filter) {
		return articleRepo.findAllFitler(filter);
	}

	@Override
	public List<Article> findByAll(ArticleFilter filter) {
		return articleRepo.findByAll(filter);
	}

	
}
