package com.kimheang.article.service;

import java.util.List;

import com.kimheang.article.model.Category;

public interface CategoryService {
	void add (Category category);
	void update(Category category);
	void delete(int id);
	List<Category> findAll();
	Category findOne(int id);
}
