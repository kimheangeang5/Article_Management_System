package com.kimheang.article.service;

import java.util.List;

import com.kimheang.article.model.Article;
import com.kimheang.article.model.ArticleFilter;
import com.kimheang.article.model.ArticlePagination;
import com.kimheang.article.model.Category;

public interface ArticleService {
	void add(Article article);
	Article findOne(int id);
	List<Article> findAll(int page);
	void update(Article article,String oldFile,boolean isDelete);
	void delete(int id);
	List<Article> findAllList();
	List<Article> findBytitle(String title);
	List<Article> findByCategory(int cateId);
	List<Article> findAllFilter(ArticleFilter filter);
	List<Article> findByAll(ArticleFilter filter);
}
