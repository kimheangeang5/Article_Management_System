package com.kimheang.article.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kimheang.article.model.Category;
import com.kimheang.article.repository.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService{
	@Autowired
    private CategoryRepository categoryRepos;
	@Override
	public void add(Category category) {
		categoryRepos.add(category);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepos.findAll();
	}

	@Override
	public Category findOne(int id) {
		return categoryRepos.findOne(id);
	}

	@Override
	public void update(Category category) {
		categoryRepos.update(category);
		
	}

	@Override
	public void delete(int id) {
		categoryRepos.delete(id);
	}

}
