package com.kimheang.article.model;

public class ArticleFilter {
	private String title;
	private Integer cateId;
	private Integer limit;
	private Integer page;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCateId() {
		return cateId;
	}
	public void setCateId(Integer cateId) {
		this.cateId = cateId;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "ArticleFilter [title=" + title + ", cateId=" + cateId + ", limit=" + limit + ", page=" + page + "]";
	}
	
}
