package com.kimheang.article.model;

import javax.validation.constraints.NotBlank;

public class Category {
	private int id;
	@NotBlank(message="category name can not be empty")
	private String Name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Category() {}
	public Category(int id, String name) {
		super();
		this.id = id;
		Name = name;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", Name=" + Name + "]";
	}
}
