package com.kimheang.article.model;

public class ArticlePagination {
	private Integer limit;
	private Integer offSet;
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffSet() {
		return offSet;
	}
	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}
	@Override
	public String toString() {
		return "ArticlePagination [limit=" + limit + ", offSet=" + offSet + "]";
	}
	
}
