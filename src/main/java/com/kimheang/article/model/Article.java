package com.kimheang.article.model;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;


public class Article {
	private int id;
	@NotEmpty(message="Title may not be empty")
	@Pattern(regexp="[^0-9]*",message="Only String allow")
	@Size(min=5,max=30)
	private String title;
	@NotEmpty(message="Description may not be empty")
	@Size(min=5,max=50)
	private String description;
	@NotEmpty(message="Author may not be empty")
	@Size(min=5,max=30)
	private String author;
	private String thumbnail;
	private Category category;
	private String createdDate;
	public Article() {
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Article(@NotNull(message = "Id may not be null") @DecimalMin("1") int id,
			@NotEmpty(message = "Title may not be empty") @Pattern(regexp = "[^0-9]*", message = "Only String allow") @Size(min = 5, max = 30) String title,
			@NotEmpty(message = "Description may not be empty") @Size(min = 5, max = 50) String description,
			@NotEmpty(message = "Author may not be empty") @Size(min = 5, max = 30) String author, String thumbnail,
			Category category, String createdDate) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.author = author;
		this.thumbnail = thumbnail;
		this.category = category;
		this.createdDate = createdDate;
	}
}
