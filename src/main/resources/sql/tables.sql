CREATE TABLE tb_categories(
 	id INT primary key auto_increment,
 	name VARCHAR not null
);

CREATE TABLE tb_articles(
	id INT primary key auto_increment,
	title VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    author VARCHAR NOT NULL,
    thumbnail VARCHAR NOT NULL,
    created_date VARCHAR NOT NULL,
    cate_id INT REFERENCES tb_categories(id) ON DELETE CASCADE ON UPDATE CASCADE
)